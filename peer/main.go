/*
Copyright IBM Corp. All Rights Reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	_ "net/http/pprof"
	"os"
	"strings"

	"bitbucket.org/changjinkim/m3/peer/chaincode"
	"bitbucket.org/changjinkim/m3/peer/channel"
	"bitbucket.org/changjinkim/m3/peer/clilogging"
	"bitbucket.org/changjinkim/m3/peer/common"
	"bitbucket.org/changjinkim/m3/peer/node"
	"bitbucket.org/changjinkim/m3/peer/version"
	"bitbucket.org/changjinkim/m3/common/flogging"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var logger = flogging.MustGetLogger("main")

// The main command describes the service and
// defaults to printing the help message.
var mainCmd = &cobra.Command{
	Use: "peer"}

func main() {

	logger.Info("main file")
	// For environment variables.
	viper.SetEnvPrefix(common.CmdRoot)
	viper.AutomaticEnv()
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	// Define command-line flags that are valid for all peer commands and
	// subcommands.
	mainFlags := mainCmd.PersistentFlags()
	mainFlags.String("logging-level", "", "Legacy logging level flag")
	viper.BindPFlag("logging_level", mainFlags.Lookup("logging-level"))
	mainFlags.MarkHidden("logging-level")

	mainCmd.AddCommand(version.Cmd())
	mainCmd.AddCommand(node.Cmd())
	mainCmd.AddCommand(chaincode.Cmd(nil))
	mainCmd.AddCommand(clilogging.Cmd(nil))
	mainCmd.AddCommand(channel.Cmd(nil))

	// On failure Cobra prints the usage message and error string, so we only
	// need to exit with a non-0 status
	if mainCmd.Execute() != nil {
		os.Exit(1)
	}
}

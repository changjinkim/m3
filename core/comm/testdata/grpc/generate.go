/*
Copyright IBM Corp. All Rights Reserved.

SPDX-License-Identifier: Apache-2.0
*/

// +build ignore

//go:generate protoc --proto_path=$GOPATH/src/bitbucket.org/changjinkim/m3/core/comm/testdata/grpc --go_out=plugins=grpc:$GOPATH/src test.proto

package grpc

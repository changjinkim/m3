// Copyright IBM Corp. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"bitbucket.org/changjinkim/m3/common/localmsp"
	"bitbucket.org/changjinkim/m3/common/tools/configtxgen/encoder"
	genesisconfig "bitbucket.org/changjinkim/m3/common/tools/configtxgen/localconfig"
	cb "bitbucket.org/changjinkim/m3/protos/common"
)

func newChainRequest(consensusType, creationPolicy, newChannelID string) *cb.Envelope {
	env, err := encoder.MakeChannelCreationTransaction(newChannelID, localmsp.NewSigner(), genesisconfig.Load(genesisconfig.SampleSingleMSPChannelProfile))
	if err != nil {
		panic(err)
	}
	return env
}

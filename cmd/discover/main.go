/*
Copyright IBM Corp. All Rights Reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"os"

	"bitbucket.org/changjinkim/m3/bccsp/factory"
	"bitbucket.org/changjinkim/m3/cmd/common"
	"bitbucket.org/changjinkim/m3/discovery/cmd"
)

func main() {
	factory.InitFactories(nil)
	cli := common.NewCLI("discover", "Command line client for fabric discovery service")
	discovery.AddCommands(cli)
	cli.Run(os.Args[1:])
}
